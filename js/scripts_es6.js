"use strict";

// Language switcher
function languageSwitcher() {
    let languageContainer =  $( ".js-language-container" ),
        languageList = $( ".js-languages-list" ),
        languageListItems = $( ".js-languages-list li" );       

        languageContainer.click(function(e) {
            let classToHide = ".js-languages-list li." + languageContainer.data("language");

            e.preventDefault();
        
            languageList.toggle();
            $( classToHide ).hide();      
        });

        languageListItems.click(function(e) {
            let activeLanguageName = $( this ).html(),
                activeLanguageShortName = $( this ).data("language");
        
            e.preventDefault();
        
            languageContainer.html(activeLanguageName)
                    .attr("class", "language js-language-container js-modal-opener")
                    .addClass(activeLanguageShortName)
                    .data("language", activeLanguageShortName);
        
            languageList.toggle();
        
            languageListItems.show();
        });

        closeModalWindow(languageList);
};

function phoneActivity() {
    let phonenumber = $(".service-link.phonenumber"),
        email = $(".service-link.email"),
        currenHours = new Date().getHours();
            
        if (currenHours >= 9 && currenHours < 18) {
            phonenumber.show();
            email.hide();
        } else {
            phonenumber.hide();
            email.show();
        }
};

function minicartFunctionality() {
    let minicartIcon = $( ".js-minicart a" ),
        minicartBlock = $( ".js-minicart-block" );

    minicartIcon.click(function(e) {
        e.preventDefault();
        minicartBlock.toggle();
    });

    closeModalWindow(minicartBlock);
};


function playVideo () {
    let video = $('.js-video'),
        playBtn = $('.js-video-play-btn');
    
    playBtn.click(() => {
        playBtn.hide();
        video[0].play();
    });

    video.on("ended", function() {
        playBtn.show();
    });
}

function closeModalWindow (window) {
    let modalOpenerClass = '.js-modal-opener',
    modalOpener = $(modalOpenerClass);
    
    $(document).mouseup(function(e) {
        if (!window.is(e.target) 
                && !modalOpener.is(e.target) 
                && !$(e.target).parents(modalOpenerClass).length 
                && window.has(e.target).length === 0) {
                    window.hide();
        }
    });
};

function popularCategoriesFunctionality() {
    let categoryBtns = $('.popular-categories li'),
        activeClass = 'active';

    categoryBtns.click(function(){
        let category = $(this).data('category');
        renderCategory(data[category]);
        categoryBtns.removeClass(activeClass);
        $(this).addClass(activeClass);
    });
};


//Initialization of all functionality
$(function() {
    languageSwitcher();
    phoneActivity();
    setInterval(phoneActivity, 300000);
    minicartFunctionality();
    playVideo();
    renderCategory(data.category_1);
    popularCategoriesFunctionality();
});