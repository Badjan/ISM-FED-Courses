"use strict";

//Database part
let data = { 
    category_1 : [
                    {
                        labelType: 'new',
                        discountAmount: '',
                        imageURL: 'images/category_1/product_img_1.png',
                        name: 'Lorem Ipsum',
                        price: '24,95',
                        productURL: '#'
                    }, {
                        labelType: '',
                        discountAmount: '',
                        imageURL: 'images/category_1/product_img_2.png',
                        name: 'Lorem Ipsum is simply dummy text',
                        price: '24,95',
                        productURL: '#'
                    },{
                        labelType: '',
                        discountAmount: '',
                        imageURL: 'images/category_1/product_img_3.png',
                        name: 'Lorem Ipsum is simply dummy text',
                        price: '39,95',
                        productURL: '#'
                    },{
                        labelType: 'discount',
                        discountAmount: '10',
                        imageURL: 'images/category_1/product_img_4.png',
                        name: 'Lorem Ipsum is simply dummy text',
                        price: '835,-',
                        productURL: '#'
                    },{
                        labelType: '',
                        discountAmount: '',
                        imageURL: 'images/category_1/product_img_5.png',
                        name: 'Lorem Ipsum is simply dummy text',
                        price: '111,50',
                        productURL: '#'
                    },{
                        labelType: '',
                        discountAmount: '',
                        imageURL: 'images/category_1/product_img_3.png',
                        name: 'Lorem Ipsum is simply dummy text',
                        price: '39,95',
                        productURL: '#'
                    }
                ],
    category_2 : [
                    {
                        labelType: 'new',
                        discountAmount: '',
                        imageURL: 'images/category_2/product_img_1.png',
                        name: 'product 1',
                        price: '9,95',
                        productURL: '#'
                    }, {
                        labelType: '',
                        discountAmount: '',
                        imageURL: 'images/category_2/product_img_2.png',
                        name: 'Lorem Ipsum is simply product 2',
                        price: '24,95',
                        productURL: '#'
                    },{
                        labelType: '',
                        discountAmount: '',
                        imageURL: 'images/category_2/product_img_3.png',
                        name: 'Product 3 Lorem Ipsum is simply',
                        price: '19,95',
                        productURL: '#'
                    },{
                        labelType: '',
                        discountAmount: '',
                        imageURL: 'images/category_2/product_img_4.png',
                        name: 'Lorem Ipsum is simply product 4',
                        price: '409,99',
                        productURL: '#'
                    },{
                        labelType: 'discount',
                        discountAmount: '20',
                        imageURL: 'images/category_2/product_img_5.png',
                        name: 'Lorem Ipsum is simply product 5',
                        price: '11,50',
                        productURL: '#'
                    },{
                        labelType: 'new',
                        discountAmount: '',
                        imageURL: 'images/category_2/product_img_6.png',
                        name: 'Lorem Ipsum is simply product 6',
                        price: '199,95',
                        productURL: '#'
                    }
            ],
category_3 : [
                {
                    labelType: 'discount',
                    discountAmount: '5',
                    imageURL: 'images/category_3/product_img_1.png',
                    name: 'Category 3 product 1',
                    price: '9,95',
                    productURL: '#'
                }, {
                    labelType: '',
                    discountAmount: '',
                    imageURL: 'images/category_3/product_img_2.png',
                    name: 'Category 3 product 2',
                    price: '24,95',
                    productURL: '#'
                },{
                    labelType: 'new',
                    discountAmount: '',
                    imageURL: 'images/category_3/product_img_3.png',
                    name: 'Category 3 product 3',
                    price: '19,95',
                    productURL: '#'
                },{
                    labelType: '',
                    discountAmount: '',
                    imageURL: 'images/category_3/product_img_4.png',
                    name: 'Category 3 product 4',
                    price: '409,99',
                    productURL: '#'
                },{
                    labelType: 'new',
                    discountAmount: '',
                    imageURL: 'images/category_3/product_img_5.png',
                    name: 'Category 3 product 5',
                    price: '11,50',
                    productURL: '#'
                },{
                    labelType: '',
                    discountAmount: '',
                    imageURL: 'images/category_3/product_img_6.png',
                    name: 'Category 3 product 6',
                    price: '199,95',
                    productURL: '#'
                }
            ],
category_4 : [
            {
                labelType: 'discount',
                discountAmount: '5',
                imageURL: 'images/category_4/product_img_1.png',
                name: 'Category 4 product 1',
                price: '9,95',
                productURL: '#'
            }, {
                labelType: 'new',
                discountAmount: '',
                imageURL: 'images/category_4/product_img_2.png',
                name: 'Category 4 product 2',
                price: '24,95',
                productURL: '#'
            },{
                labelType: 'discount',
                discountAmount: '10',
                imageURL: 'images/category_4/product_img_3.png',
                name: 'Category 4 product 3',
                price: '19,95',
                productURL: '#'
            },{
                labelType: '',
                discountAmount: '',
                imageURL: 'images/category_4/product_img_4.png',
                name: 'Category 4 product 4',
                price: '409,99',
                productURL: '#'
            },{
                labelType: 'discount',
                discountAmount: '15',
                imageURL: 'images/category_4/product_img_5.png',
                name: 'Category 4 product 5',
                price: '11,50',
                productURL: '#'
            },{
                labelType: '',
                discountAmount: '',
                imageURL: 'images/category_4/product_img_6.png',
                name: 'Category 4 product 6',
                price: '199,95',
                productURL: '#'
            }
        ]
    };


//Popular cetegories scripts
function createPopularProducts (pruductsArr) {
    let result = "",
        products = pruductsArr,
        productsQty = products.length,
        i = 0;

    for (; i < productsQty; i++) {
        result += `<li class="column-20 product-item">`;
        let template = `<div class="item-image">
                            <a href="#">
                                <img class="item-image content-image" src="${products[i].imageURL}" alt="${products[i].name}">
                            </a>
                        </div>
                        <div class="item-description">
                            <a href="#">
                                <span class="item-name">${products[i].name}</span>
                            </a>
                            <div class="price">€&nbsp;${products[i].price}</div>
                        </div>
                        <div class="item-links">
                            <div class="add-to-cart"><span>In shopping cart</span></div>
                            <div class="item-link">
                                <a href="${products[i].productURL}">More info</a>
                            </div>
                        </div>`;
            
        result += template;

        switch(products[i].labelType) {
            case 'new':
                let labelNew = `<div class="label label-new">
                                <span class="label-text">New</span>
                            </div>`;
                result += labelNew;
                break;
            case 'discount':
                let labelDiscount = `<div class="label label-discount">
                                <span class="label-text">${products[i].discountAmount}% discount</span>
                            </div>`;
                result += labelDiscount;
                break;
            default:
                //no labels present;
        }

        result += `</li>`;

    }
    
    return result;
};

function renderCategory(category) {
    let popularProducts = createPopularProducts(category);
    $('.popular-products-container .owl-carousel').trigger('replace.owl.carousel', popularProducts).trigger('refresh.owl.carousel');
};