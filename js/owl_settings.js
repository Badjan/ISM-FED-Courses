$('.banner-main.owl-carousel').owlCarousel({
    loop: true,
    margin: 10,
    dots: false,
    items: 1,
    nav: true
})

$('.seo-info-block.owl-carousel').owlCarousel({
    loop: false,
    dots: false,
    nav: false,
    items: 3,
    responsive:{
        0:{
            loop: true,
            autoplay: true,
            items: 1
        },
        768:{
            loop: true,
            autoplay: true,
            items: 2
        },
        1024:{
            autoplay: false,
            margin: 10,
        }
    }
})

$('.popular-products-container ul.owl-carousel').owlCarousel({
    loop: true,
    margin: 0,
    dots: false,
    nav: true,
    items: 5,
    responsive:{
        0:{
            items: 1,
        },
        768:{
            items: 3
        },
        1024:{
            items: 5,
        }
    }
})